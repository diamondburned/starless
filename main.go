package main

import (
	"log"
	"net/url"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"

	"github.com/RumbleFrog/discordgo"
)

var (
	d *discordgo.Session

	webhook      *discordgo.Webhook
	webhookID    int64
	webhookToken string
	webhookChID  int64

	debug = func() bool {
		if os.Getenv("DEBUG") != "" {
			return true
		}

		return false
	}()

	// Star is the string for Emoji
	Star = "⭐"

	// Count is the number of Stars it should have before posting
	Count = 3
)

func init() {
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)
}

func main() {
	var err error

	// Get the $TOKEN env var
	token := os.Getenv("TOKEN")
	if token == "" {
		log.Fatalln("No $TOKEN")
	}

	// Get the $WEBHOOK_URL env var
	whURL := os.Getenv("WEBHOOK_URL")
	if whURL == "" {
		log.Fatalln("No $WEBHOOK_URL")
	} else {
		// Parsing the Webhook URL
		u, err := url.Parse(whURL)
		if err != nil {
			log.Fatalln("Error parsing URL", whURL, err)
		}

		// Check if host is discordapp.com
		// for URLs copied straight out of the webhooks dialog, it
		// should be
		if u.Host != "discordapp.com" {
			log.Fatalln("Invalid host:", u.Host+"!")
		}

		// Split the path, delimiter /
		paths := strings.Split(u.Path, "/")

		/*
			Paths (len=5)
			|-
			|- api
			|- webhooks
			|- 539313130283466765
			|- Qz-Lex1RXxXVyUwoHb9PUHphKnCJsak6hwtznP3fBRCmU09k12H_UCqUGCEi-nn2qrmH
		*/

		// 1st is actually a white-space! Who would've thought!
		// check above for parts
		if len(paths) != 5 {
			log.Fatalln("Invalid URL path!", paths)
		}

		// check if the url is really a webhook url
		if paths[1] != "api" || paths[2] != "webhooks" {
			log.Fatalln("Invalid URL path!")
		}

		/*
			ok look bud

			  I know I'm doing a lot of checks, and I don't need to.
			You know, I've been intentionally breaking a lot of things
			on my own, just so I could test if things work or not. I have
			come to the conclusion, that the average user isn't that smart.
			You can't expect them to know what a webhook URL is. Why don't
			we just flat out reject it? Make them Google, or something.

			/rant end
		*/

		// parse the webhook ID into int64 from string
		webhookID, err = strconv.ParseInt(paths[3], 10, 64)
		if err != nil {
			// man, I miss Bash
			log.Fatalln(err)
		}

		// pass in the token as-is
		webhookToken = paths[4]
	}

	if a := os.Getenv("STAR"); a != "" {
		Star = a
	}

	if a := os.Getenv("COUNT"); a != "" {
		i, err := strconv.Atoi(a)
		if err != nil {
			log.Fatalln(err)
		}

		Count = i
	}

	// Make a dummy session thing with the token as-is
	d, err = discordgo.New("Bot " + token)
	if err != nil {
		log.Fatalln(err)
	}

	// Set the max messages stored in state cache, the more the merrier
	d.State.MaxMessageCount = 150

	// Add the on-reaction-add handler to query stars
	d.AddHandler(onReactionAdd)

	// Connect to Discord's gateway
	if err = d.Open(); err != nil {
		// Exit if fails
		log.Fatalln(err)
	}

	verbose("Bot is ready")

	// Close the connection when the app exits
	defer d.Close()

	// Query webhook info
	webhook, err = d.WebhookWithToken(webhookID, webhookToken)
	if err != nil {
		// Exit if webhook is invalid/query failed
		log.Fatalln(webhookChID)
	}

	// Store webhook's channel ID as a global variable
	// (this is used elsewhere)
	webhookChID = webhook.ChannelID

	// Block the process from exiting until a
	//    1. SIGINT
	//    2. SIGTERM
	//    3. Interrupt/Kill signals
	// are received
	sc := make(chan os.Signal, 1)
	signal.Notify(sc, syscall.SIGINT, syscall.SIGTERM, os.Interrupt, os.Kill)
	<-sc
}

func verbose(i ...interface{}) {
	if debug {
		log.Println(i...)
	}
}
