# starless

Starless is a simple Starboard, designed with no databases in mind.

![preview](https://cdn.discordapp.com/attachments/361910177961738244/539366189927890944/unknown.png)

*For the classic embed-style, refer to `embed-mode` branch*

## Why?

~~Why not?~~

I've tried a lot of Starboard bots, and I'm quite fed up with how buggy they are. An example would be spamming the star reaction, so that it would spam a lot of embeds in the starboard.

I've tried writing one myself (in fact, it was my first ever Go project), though the code was really crappy, and I felt like I didn't need all the fancy stuff it has. I've also thought about databases, and decided not to use one, as I don't need one, and I don't want to rely on another media of storage (Discord is one, and it works fine).

Anyway, this bot is the answer to the issues above. It is a simple bot, designed to just put a message to the starboard cleanly. It doesn't glitch out, it doesn't even remove the message when the reaction reaches below the threshold (I could implement this, but it's not in my goal).

The -less suffix in the name is inspired from [suckless](https://suckless.org/). (Yes, I know this bot is not written in C. Just shut up.) I've decided to go with this name, since my objective for this bot matches that of suckless':

>Our philosophy is about keeping things simple, minimal and usable.

## How does it work?

Instead of using a persistent database, Starless would check against the target channel for existing messages. As long as the Starboard doesn't fill up quickly, it should work fine.

## Configurables

~~Star count is stored in `config.go` in a constant. Change the constant, then recompile.~~

- Star emoji: `$STAR`
- Star count: `$COUNT` (`COUNT=4` for example)

## How to install

1. `go build` or grab the statically compiled binary from the latest CI artifact
1. `export TOKEN="your bot token (without the Bot prefix, that's added for you)"`
2. `export WEBHOOK_URL="your webhook URL"`
3. `./starless`

