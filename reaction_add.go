package main

import (
	"fmt"
	"log"

	"github.com/RumbleFrog/discordgo"
)

func onReactionAdd(s *discordgo.Session, ra *discordgo.MessageReactionAdd) {
	verbose("New message! ID:", ra.MessageID)
	// spew.Dump(ra)
	msg, err := d.State.Message(ra.ChannelID, ra.MessageID)
	if err != nil {
		verbose("Failed to get message in the state cache, now getting it from the server")
		msg, err = d.ChannelMessage(ra.ChannelID, ra.MessageID)
		if err != nil {
			verbose("Failed to fetch anything. Dropping message.")
			log.Fatalln(err)
		}
	}

	checkReaction(msg)
}

func checkReaction(m *discordgo.Message) {
	// nil by default
	var starReaction *discordgo.MessageReactions

	verbose("checking if the message appears in any of the existing pinned ones")
	if b, err := InStore(m.ID); err != nil {
		// if it fails to do so
		log.Println(err)
		return
	} else if b {
		// if it exists
		verbose("Message exists in target channel, dropping")
		return
	}

	for _, r := range m.Reactions {
		if r.Emoji != nil && r.Emoji.Name == Star {
			// no longer nil, found the right reaction
			starReaction = r
			break
		}
	}

	// if above statement fails to find any matched reactions
	if starReaction == nil {
		verbose("can't find the right emoji, dropping")
		return
	}

	// if the count doesn't match the one configured
	if starReaction.Count != Count {
		verbose("starcount doesn't match:", starReaction.Count)
		return
	}

	// if no guildID in message
	if m.GuildID == 0 {
		// check the state channel first for performance
		c, err := d.State.Channel(m.ChannelID)
		if err != nil {
			// if err/not in state, query the server
			c, err = d.Channel(m.ChannelID)
			if err != nil {
				// can't do anything anymore
				return
			}
		}

		// stores the GuildID
		m.GuildID = c.GuildID
	}

	// if the message came from a different guild
	if m.GuildID != webhook.GuildID {
		verbose("different guildid, dropping")
		return
	}

	// get the display name and role color
	nickname, color := getUserData(m)

	// actual webhook content
	webhook := &discordgo.WebhookParams{
		Username: nickname,
		Content: fmt.Sprintf(
			// message (source)
			"%s [_(source)_](<https://discordapp.com/channels/%d/%d/%d>)",
			m.Content, m.GuildID, m.ChannelID, m.ID,
		),
		AvatarURL: fmt.Sprintf(
			// avatar, size 128
			"https://cdn.discordapp.com/avatars/%d/%s.png?size=128",
			m.Author.ID, m.Author.Avatar,
		),
		/*
			since Discord decided to be an ass with the way webhooks handle
			attachments, I'm using embeds to display images

			as of now, this ignores embed files, leaving only images
		*/
		Embeds: func() (embeds []*discordgo.MessageEmbed) {
			for _, a := range m.Attachments {
				// if the embed is an actual image
				if a.Width > 0 && a.Height > 0 {
					embeds = append(
						embeds, &discordgo.MessageEmbed{
							Color: color,
							Image: &discordgo.MessageEmbedImage{
								URL: a.URL,
							},
						},
					)
				}
			}

			return
		}(),
	}

	// attempts to send the embed
	if err := d.WebhookExecute(webhookID, webhookToken, true, webhook); err != nil {
		// warn on the channel if it fails
		if _, err2 := d.ChannelMessageSend(m.ChannelID, "Error! / "+err.Error()); err2 != nil {
			// if all means failed, just log it and get out
			log.Println(err, err2)
		}
	}
}
