package main

import (
	"strconv"
	"strings"

	"github.com/RumbleFrog/discordgo"
)

// InStore ..
func InStore(msgID int64) (b bool, err error) {
	var msgs []*discordgo.Message

	// check the state cache for current channel
	// this should contain past messages in the channel
	channel, err := d.State.Channel(webhookChID)
	if err != nil || len(channel.Messages) < 1 {
		// if no messages in the channel, fetch from server
		// pretty sure this stores the thing in state cache
		msgs, err = d.ChannelMessages(webhookChID, 100, 0, 0, 0)
		if err != nil {
			// if all means fail, quit (b defaulted to false)
			return
		}

	} else {
		// if state cache actually worked
		msgs = channel.Messages

		/*
			If this ever works, it'll exit before the code in
			reaction_add.go does. Just the tiny optimization
			I could get, but hey
		*/
		if channel.GuildID != webhook.GuildID {
			return false, nil
		}
	}

	/*
		shitty hack to check if the (source) link contains the ID

		basically, instead of parsing the link properly (split, parseint, ==),
		we're just seeing if the ID as a string is in the URL. If it is, it is
	*/
	idString := strconv.FormatInt(msgID, 10)

	for _, m := range msgs {
		_a := strings.Split(m.Content, "(")
		if len(_a) > 0 {
			// string's in!
			//                     v [-1]
			if strings.Contains(_a[len(_a)-1], idString) {
				return true, nil
			}
		}
	}

	// string's not
	return false, nil
}
